# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Chat Room
* Key functions (add/delete)
    1. Personal chat room
    2. Group chat room
    3. Load message history
    4. Sign in/Log in/Log out
    5. Chat with new user
    
* Other functions (add/delete)
    1. Change password by sending email
    2. Send vertification email after signup

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-71e19.firebaseapp.com/

# Components Description : 
1. Personal chat room: 點取首頁的personal chat進入個人聊天室，即可選擇任一在此網站註冊之用戶開啟個人聊天室(Home Page)
2. Group chat room: 點取首頁的Group chat進入個人聊天室，與全站用戶聊天(Home Page)
3. Load message history: Group chat和Personal chat之中可以載入先前聊天紀錄
4. Sign in/Log in/Log out: 基本登入註冊登出功能
5. Chat with new user: 新註冊之用戶顯現在個人聊天室的userlist，點擊即可開啟聊天
6. Third-Party Sign In:可以google帳戶做為第三方登入
7. Chrome Notification: 登入後若有人傳訊息即可看到通知
8. CSS Animation: 剛進入主頁大標題的滑動，滑到定點即停止

# Other Functions Description(1~10%) : 
1. Change password by sending email: 登入後若想更改密碼，至首頁Settings中change password裡的按鈕點擊後到信箱收取更改密碼之信件

2. Send vertification email after signup: 使用email註冊後將驗證信寄至信箱

## Security Report (Optional)
聊天室裡的訊息發出前先檢查是否為html code，否則將出現error messsage，以此避免他人能利用message的功能改變了網站的樣式及架構
